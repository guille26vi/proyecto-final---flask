#librerias      
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
from datetime import datetime


# initializations
app = Flask(__name__)
"""inicializacion del aplicativo de Flask"""

# Mysql Connection
app.config['MYSQL_HOST'] = 'localhost' 
app.config['MYSQL_USER'] = 'admin'
app.config['MYSQL_PASSWORD'] = 'admin'
app.config['MYSQL_DB'] = 'proyecto'
mysql = MySQL(app) #conexion del app


# settings
app.secret_key = "mysecretkey"

# routes
@app.route('/') 
def Index():
    #creamos cursor de la tabla ASISTENCIAS para crear la tabla
        cur = mysql.connection.cursor()
        cur.execute('SELECT a.codigo,a.nombre,a.apellido,b.descripcion,a.fecha_sistema,a.hora FROM asistencias a, carreras b where a.carrera = b.codigo order by a.codigo desc limit 0,5')
        data = cur.fetchall()
        cur.close() 
    #creamos cursor de la tabla de CARRERAS para llenar la lista de seleccion 
        cur = mysql.connection.cursor()
        cur.execute('SELECT * FROM carreras')
        lista = cur.fetchall()
        cur.close()
        return render_template('index.html', asistencia = data,carrers = lista) #para salir hacia el Index

@app.route('/Registro', methods=['POST'])
def Registro():
    if request.method == 'POST':
        fullname = request.form['fullname'] #valor del momento
        lastname = request.form['lastname']        
        correo = request.form['correo']
        carr = request.form['carrera']
        now = datetime.now()
        formatted_date = now.strftime('%Y-%m-%d') #formato de fecha Year-Month-Day
        formatted_hour = now.strftime('%H:%M %p') #formato de hora
        print(formatted_hour)
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO asistencias(nombre, apellido,correo,carrera,fecha_sistema,hora) VALUES (%s,%s,%s,%s,%s,%s)", (fullname,lastname,correo,carr,formatted_date,formatted_hour))
        mysql.connection.commit()
        flash('Se ha Registrado su Asistencia Corectamente!')
        return redirect(url_for('Index')) #vuelve al Index

@app.route('/delete/<string:id>', methods = ['POST','GET'])
def delete_contact(id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM asistencias WHERE codigo = {0}'.format(id))
    mysql.connection.commit()
    flash('Registro eliminado Correctamente!')
    return redirect(url_for('Index'))

# Se renderiza la pagina de Estadisticas
@app.route('/resumen')
def resumen():
    cur = mysql.connection.cursor()
    cur.execute('select count(a.codigo),b.descripcion  FROM asistencias a, carreras b where a.carrera = b.codigo group by b.Descripcion order by a.codigo desc')
    data = cur.fetchall()
    cur.close()
    return render_template("resumen.html",tot = data)



#Creamos la route para la pagina de Sobre Nosotros
@app.route('/sobre_nosotros')
def sobre():
    
    return render_template('sobre_nosotros.html')


# starting the app
if __name__ == '__main__':
    app.run(host='127.0.0.1',port = 3000, debug = True)

